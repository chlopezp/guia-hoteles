
module.exports = function(grunt) {

 // Project configuration.
 grunt.initConfig({
 // Task configuration.
	 sass: {
		 dist: {
		 options: {
		 style: 'expanded'
		 },
		 files: {
		 'css/styles.css': 'css/styles.scss'
		 }
		 }
	 },
	 
	 watch:{
		 files: ['css/*.scss'],
		 tasks: ['css']
	 },
	 
	 browserSync:{
		 dev:{
			 bsFiles:{
				 src:[
					'css/*.css',
					'*.html',
					'js/*.js'
				 ]
		 },
		 options:{
			watchTask: true,
			server:{
				baseDir: './'
			}		
		 }
		 
		}
	 },

	 imagemin:{
		 dynamic:{
			 files:[{
				 expand: true,
				 cwd: './',
				 src: 'images/*.{png,gif,jpg,jpeg}',
				 dest: 'dist/'
			 }]
		 }
	 },

	 copy:{
		 html:{
			 files:[{
				expand: true,
				dot: true,
				cwd: './',
				src: ['*.html'],
				dest: 'dist'	
			}]
		 }
	 },
	 
	 clean:{
		 build:{
			 src: ['dist/']
		 }
	 },
	 
	 cssmin:{
		 dist:{}
	 },
	 
	 uglify:{
		 dist:{}
	 },
	 
	 filerev:{
		 options:{
			 encoding: 'utf8',
			 algorithm: 'md5',
			 length: 20
		 },
		 
		 release:{
			 
			 files: [{
				src:[
					'dist/js/*.js',
					'dist/css/*.css',
				]
			 }]
		 }
	 },
	 
	 concat:{
		 options:{
			 separator: ';'
		 },
		 dist:{}
	 },
	 
});
	grunt.loadNpmTasks('grunt-contrib-watch');	
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');	
	grunt.registerTask('css', ['sass']);
	grunt.registerTask('default', ['browserSync', 'watch']);

};

