module.exports = funtion(grunt){
	grunt.initConfig({
		sass:{
			dist:{
				options: {                       // Target options
				style: 'expanded'
				},
				files:[{
					expand: true,
					cwd: ['css'],
					src: '*.scss',
					dest: 'css',
					ext: '.css'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');	
	grunt.registerTasks('css', ['sass']);
};

